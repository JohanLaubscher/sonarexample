import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import {routes} from "./app-routing.module";

describe('AppComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        AppComponent
      ],
    }).compileComponents();
    spyOn(console, "log");
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it('routes should be empty?', () => {
    expect(routes.length === 0).toBeTruthy();
  });

  it('should log in console when title is "2"',() => {
      const fixture = TestBed.createComponent(AppComponent);
      const app = fixture.componentInstance;
      app.title = '2';
      app.logText();
      expect(console.log).toHaveBeenCalledWith('Broken' as any)
  });

  it('should not log in console when title is left to default',() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(console.log).not.toHaveBeenCalled();
  });

  it(`should have as title 'sonarexample'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('sonarexample');
  });

  it('should render title', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('.content span').textContent).toContain('sonarexample app is running!');
  });
});
