import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'sonarexample';

  constructor() {
    this.logText();
  }

  logText() {
    if(this.title === '2'){
      console.log('Broken')
    }
  }
}
